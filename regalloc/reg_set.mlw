module Set
       use export int.Int
       use export list.List
       use export list.Mem
       type t 'a
       val function appt 'a (t 'a) : bool
       val function card (t 'a) : int
       	   ensures { result >= 0 }
       val function empty : t 'a
       	   ensures { forall v. appt v result = False }
       val function add (v : 'a) (s : t 'a) : t 'a
       	   requires { not (appt v s) }
       	   ensures { forall v'. appt v' result = (appt v' s \/ v = v') }
	   ensures {  card result = card s + 1 }
       val function remove (v : 'a) (s : t 'a) : t 'a
       	   requires { appt v s }
       	   ensures { forall v'. appt v' result = (appt v' s /\ v <> v') }
	   ensures { card result = card s - 1 }
       val function union (s1 : t 'a) (s2 : t 'a) : t 'a
       	   ensures { forall v. appt v result = (appt v s1 \/ appt v s2) }
       exception Not_found
       val function diff (s1 : t 'a) (s2 : t 'a) : t 'a
       	   ensures { forall v. appt v result = (appt v s1 /\ not (appt v s2)) }
       val function to_list (s : t 'a) : list 'a
       	   ensures { forall v. mem v result = appt v s }
       let choose (s : t 'a) : 'a
       	   ensures { appt result s }
	   raises { Not_found -> forall v. appt v s = False }
       =
	   match to_list s with
	   | Nil -> raise Not_found
	   | Cons x _ -> x
	   end
       let function sadd (v : 'a) (s : t 'a) : t 'a
       	   ensures { if appt v s then result = s else result = add v s }
       =
       	   if appt v s then s else add v s
       let function sremove (v : 'a) (s : t 'a) : t 'a
       	   ensures { if appt v s then result = remove v s else result = s }
       =
	   if appt v s then remove v s else s


end

module Dic
     use export option.Option
     use Set
     type key
     type t 'a = abstract { find_opt : key -> option 'a }


     exception Not_found
     val find (k : key) (d : t 'a) : 'a
     	 ensures { d.find_opt k = Some result }
	 raises { Not_found -> d.find_opt k = None }

     val function domain (d : t 'a) : Set.t key
     	 ensures { forall k. appt k result <-> (exists v. d.find_opt k = Some v) }
     val init (r : Set.t key) : t key
     	 ensures { forall k. appt k r -> result.find_opt k = Some k }
     	 ensures { forall k. not (appt k r) -> result.find_opt k = None }
     val function add_binding (k : key) (v : 'a) (d : t 'a) : t 'a
     	 ensures { result.find_opt k = Some v }
	 ensures { forall k'. k' <> k -> result.find_opt k' = d.find_opt k' }
	 ensures { match d.find_opt k with
	 	   | None -> domain result = add k (domain d)
		   | Some _ -> domain result = domain d
		   end }

     val function remove_key (k : key) (d : t 'a) : t 'a
     	 ensures { result.find_opt k = None }
	 ensures { forall k'. k <> k' -> result.find_opt k' = d.find_opt k'}
	 ensures { match d.find_opt k with
	 	   | None -> domain result = domain d
		   | Some _ -> domain result = remove k (domain d)
		   end}
     val function map (f : 'a -> 'b) (g : t 'a) : t 'b
     	 ensures { forall k. match g.find_opt k with
       	       	 	       | None -> result.find_opt k = None
			       | Some v -> result.find_opt k = Some (f v)
			     end }
         ensures { domain result = domain g }
     let function  appt_dic (v : 'a) (g : t (Set.t 'a)) (u : key) : bool
     	 ensures { result <-> (match g.find_opt u with
	 	   	      | None -> False
			      | Some gu -> appt v gu
			      end) }
     =
	 try appt v (find u g)
	 with Not_found -> False end

     lemma no_apt_domain :
     	   forall k. forall g : t 'a. appt k (domain g) = False <-> g.find_opt k = None

end

module Regalloc
  type var
  use Set
  clone Dic with type key = var


  let function replace (v : 'a) (u : 'a) (s : Set.t 'a) : Set.t 'a
      requires { v <> u }
      ensures { appt u result = (appt u s \/ appt v s) }
      ensures { appt v result = False }
      ensures { forall w. w <> u -> w <> v -> appt w result = appt w s }
  =
      if appt v s
      then sadd u (remove v s)
      else s

  val all_from (s : Set.t var) (a : Dic.t var) : Set.t var
    ensures { forall v. appt v result <-> (exists k. appt k s /\ a.find_opt k = Some v) }


  let remove (v : var) (g : Dic.t (Set.t var)) : Dic.t (Set.t var)
      requires { match g.find_opt v with
      	       	 | None -> false
		 | Some _ -> true end }
      ensures { result.find_opt v = None }
      ensures { forall w. w <> v ->
      	      	match g.find_opt w with
      	      	| None -> result.find_opt w = None
		| Some gw -> result.find_opt w = Some (sremove v gw) end }
      ensures { domain result = remove v (domain g)}
  =
      let remv = Dic.remove_key v g in
      Dic.map (sremove v) remv

  let merge (v : var) (u : var) (g : Dic.t (Set.t var)) : Dic.t (Set.t var)
      requires { v <> u }
      requires { match g.find_opt u, g.find_opt v with
		 | Some _, Some _ -> true
		 | _ -> false end }
      ensures { result.find_opt v = None }
      ensures { match result.find_opt u, g.find_opt u, g.find_opt v with
      	      	| Some ru, Some gu, Some gv ->
		   forall e. appt e ru = (appt e (replace v u gu) \/ appt e (replace v u gv))
      		| _ -> false end }
      ensures { forall w. w <> u -> w <> v ->
      	      	match g.find_opt w with
      		| None -> result.find_opt w = None
      		| Some gw -> result.find_opt w = Some (replace v u gw) end }
      ensures { domain result = remove v (domain g) }

  =
      let g = Dic.map (replace v u) g in
      let gu = try find u g with Not_found -> Set.empty end in
      let gv = try find v g with Not_found -> Set.empty end in
      let g = Dic.add_binding u (union gu gv) g in
      Dic.remove_key v g



  let available (s : Set.t var) (a : Dic.t var) (r : Set.t var) : var
      raises  { Set.Not_found -> forall u. appt u r ->
      	      		      	 exists v. appt v s /\ a.find_opt v = Some u }
      ensures { appt result r /\
      	      	forall v av. appt v s -> a.find_opt v = Some av -> result <> av }
  =
      let set = diff r (all_from s a) in
      choose set



  val pick (g : Dic.t (Set.t var)) (r : Set.t var) : option var
      ensures { let dgr = diff (domain g) r in
      	      	match result with
      	      	| None -> forall v. not (appt v dgr)
		| Some v -> appt v dgr
		end }

  val coalesce (g : Dic.t (Set.t var)) (v : var) : option var
      ensures { match result with
      	      	| None -> True
		| Some u -> u <> v /\ appt u (domain g) /\ not (appt_dic v g u)
		end }

  predicate no_collision (g : Dic.t (Set.t var)) (a : Dic.t var) =
    forall u v au av.
    	   appt_dic v g u ->
    	   a.find_opt u = Some au ->
	   a.find_opt v = Some av ->
	   au <> av

  predicate irrefl (g : Dic.t (Set.t var)) =
    forall u gu. g.find_opt u = Some gu -> not (appt u gu)

  predicate sym (g : Dic.t (Set.t var)) =
    forall u v. appt_dic u g v -> appt_dic v g u

  lemma replace_appt :
  	forall s : Set.t 'a. forall v u w. u <> v -> w <> v -> appt w s -> appt w (replace v u s)

  let rec irc (g : Dic.t (Set.t var)) (r : Set.t var) : Dic.t var
      requires { forall v. appt v r -> appt v (domain g) }
      requires { irrefl g }
      requires { sym g }
      variant { card (domain g) }
      ensures { forall v u. result.find_opt v = Some u -> appt u (domain g) }
      ensures { forall k. appt k (domain result) = appt k (domain g) }
      raises { Dic.Not_found -> false }
      ensures { forall k. appt k r -> result.find_opt k = Some k }
      ensures { no_collision g result }
  =
      match pick g r with
      | None -> Dic.init r
      | Some v -> match coalesce g v with
      	       	  | None ->
  		    let s = Dic.find v g in
		    let a = irc (remove v g) r in
		    try Dic.add_binding v (available s a r) a
		    with Set.Not_found -> Dic.add_binding v v a end
		  | Some u ->
  		    let a = irc (merge v u g) r in
  		    Dic.add_binding v (Dic.find u a) a
		  end
      end


end
