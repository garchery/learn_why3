module Dic
     use export option.Option
     use set.Fset
     type key
     type t 'a = abstract { find_opt : key -> option 'a }

     exception Not_found
     val find (k : key) (d : t 'a) : 'a
     	 ensures { d.find_opt k = Some result }
	 raises { Not_found -> d.find_opt k = None }

     val function domain (d : t 'a) : set key
     	 ensures { forall k. mem k result <-> (exists v. d.find_opt k = Some v) }
     val function init (r : set key) : t key
     	 ensures { forall k. mem k r -> result.find_opt k = Some k }
     	 ensures { forall k. not (mem k r) -> result.find_opt k = None }
     val function add_binding (k : key) (v : 'a) (d : t 'a) : t 'a
     	 ensures { result.find_opt k = Some v }
	 ensures { forall k'. k' <> k -> result.find_opt k' = d.find_opt k' }
	 ensures { match d.find_opt k with
	 	   | None -> domain result = add k (domain d)
		   | Some _ -> domain result = domain d
		   end }

     val function remove_key (k : key) (d : t 'a) : t 'a
     	 ensures { result.find_opt k = None }
	 ensures { forall k'. k <> k' -> result.find_opt k' = d.find_opt k'}
	 ensures { match d.find_opt k with
	 	   | None -> domain result = domain d
		   | Some _ -> domain result = remove k (domain d)
		   end}
     val function map (f : 'a -> 'b) (g : t 'a) : t 'b
     	 ensures { forall k. match g.find_opt k with
       	       	 	       | None -> result.find_opt k = None
			       | Some v -> result.find_opt k = Some (f v)
			     end }
         ensures { domain result = domain g }
     let function  mem_dic (v : 'a) (g : t (set 'a)) (u : key) : bool
     	 ensures { result <-> (match g.find_opt u with
	 	   	      | None -> False
			      | Some gu -> mem v gu
			      end) }
     =
	 try mem v (find u g)
	 with Not_found -> False end

     lemma mem_domain :
     	   forall k. forall g : t 'a. (mem k (domain g) = False <-> g.find_opt k = None)
	   by mem k (domain g) = False -> not (exists gk. g.find_opt k = Some gk)
end

module Regalloc
       type var
       use import set.Fset as Set
       clone Dic with type key = var


       let function replace (v : 'a) (u : 'a) (s : set 'a) : set 'a
       	   requires { v <> u }
	   ensures { mem u result = (mem u s \/ mem v s) }
	   ensures { not mem v result }
	   ensures { forall w. w <> u -> w <> v -> mem w result = mem w s }
       =
	   if mem v s
	   then add u (remove v s)
	   else s


       val all_from (s : set var) (a : Dic.t var) : set var
       	   ensures { forall v. mem v result <-> (exists k. mem k s /\ a.find_opt k = Some v) }


       let function remove (v : var) (g : Dic.t (set var)) : Dic.t (set var)
       	   requires { mem v (domain g) }
           ensures { result.find_opt v = None }
      	   ensures { forall w. w <> v ->
      	      	     match g.find_opt w with
      	      	     | None -> result.find_opt w = None
		     | Some gw -> result.find_opt w = Some (remove v gw) end }
           ensures { domain result = remove v (domain g) }
       =
	   let remv = Dic.remove_key v g in
      	   Dic.map (remove v) remv


       let function merge (v : var) (u : var) (g : Dic.t (set var)) : Dic.t (set var)
       	   requires { v <> u }
      	   requires { mem v (domain g) /\ mem u (domain g) }
           ensures { result.find_opt v = None }
      	   ensures { match result.find_opt u, g.find_opt u, g.find_opt v with
      	      	     | Some ru, Some gu, Some gv ->
		       forall e. mem e ru = (mem e (replace v u gu) \/ mem e (replace v u gv))
      		     | _ -> false end }
           ensures { forall w. w <> u -> w <> v ->
      	      	     match g.find_opt w with
      		     | None -> result.find_opt w = None
      		     | Some gw -> result.find_opt w = Some (replace v u gw) end }
           ensures { domain result = Set.remove v (domain g) }
       =
	   let g = Dic.map (replace v u) g in
      	   let gu = try find u g with Not_found -> Set.empty end in
      	   let gv = try find v g with Not_found -> Set.empty end in
      	   let g = Dic.add_binding u (union gu gv) g in
      	   Dic.remove_key v g


       let available (s : set var) (a : Dic.t var) (r : set var) : option var
       	   ensures { match result with
	   	     | None -> forall u. mem u r ->
	   	       	       exists v. mem v s /\ a.find_opt v = Some u
	             | Some res -> mem res r /\
		       	    	   forall v av. mem v s ->
				   	    	a.find_opt v = Some av ->
						res <> av end}
       =
	   let set = diff r (all_from s a) in
      	   if is_empty set then None
	   else Some (choose set)

       val pick (g : Dic.t (set var)) (r : set var) : option var
       	   ensures { let dgr = diff (domain g) r in
      	      	     match result with
      	      	     | None -> forall v. not (mem v dgr)
		     | Some v -> mem v dgr
		     end }


       val coalesce (g : Dic.t (set var)) (v : var) : option var
       	   ensures { match result with
      	      	     | None -> True
		     | Some u -> u <> v /\ mem u (domain g) /\ not (mem_dic v g u)
		     end }

       predicate no_collision (g : Dic.t (set var)) (a : Dic.t var) =
       		 forall u v au av. mem_dic v g u ->
		 	       	   a.find_opt u = Some au ->
	   	 	       	   a.find_opt v = Some av -> au <> av

       predicate irrefl (g : Dic.t (set var)) =
       		 forall u gu. g.find_opt u = Some gu -> not (mem u gu)

       predicate sym (g : Dic.t (set var)) =
       		 forall u v. mem_dic u g v -> mem_dic v g u

       lemma domain_init :
       	         forall g : Dic.t (set 'a). forall r.
		 (forall v. mem v r -> mem v (domain g)) ->
		 (forall v. not mem v (diff (domain g) r)) ->
		 domain (init r) == domain g
		 by domain (init r) == r

       lemma mem_dic_remove :
  	     forall d u v x. d.find_opt x <> None ->
	     (mem_dic u (remove x d) v <-> (not v = x && not u = x && mem_dic u d v))
	     by (mem_dic u (remove x d) v -> exists dv. d.find_opt v = Some dv
	     	so mem u (Set.remove x dv)) /\
		(not v = x && not u = x && mem_dic u d v -> exists dv. d.find_opt v = Some dv
		so mem u dv so mem u (Set.remove x dv))

       lemma mem_dic_diff_merge :
       	     forall x y v u g. mem_dic x g y ->
	     	      	       not v = u -> mem v (domain g) -> mem u (domain g) ->
	     	      	       not x = v -> not y = v ->
			       mem_dic x (merge v u g) y
             by (exists gy. g.find_opt y = Some gy /\ mem x gy
	     	so mem x (replace v u gy))

       lemma replace_mem :
  	     forall s : set 'a. forall v u w.
	     u <> v -> w <> v -> mem w s -> mem w (replace v u s)

       lemma irrefl_remove :
       	     forall g v. mem v (domain g) -> irrefl g -> irrefl (remove v g)
	     by (forall u rgu. (remove v g).find_opt u = Some rgu -> mem u rgu -> false
	     	by not u = v
		so exists gu. g.find_opt u = Some gu
		   so rgu = Set.remove v gu
		   so mem u gu)

       lemma irrefl_merge :
       	     forall v u g. irrefl g -> sym g -> mem u (domain g) -> mem v (domain g) ->
	     	      	   v <> u -> not mem_dic v g u ->
	     	      	   irrefl (merge v u g)
	     by exists gu gv. g.find_opt u = Some gu /\ g.find_opt v = Some gv
	     	so (mem_dic u g v -> false by mem_dic v g u)
	     	so forall x mgx. (merge v u g).find_opt x = Some mgx -> mem x mgx -> false
	     	   by not x = v
		   so if x = u
		      then not mem u (replace v u gu) /\ not mem u (replace v u gv)
		      else exists gx. g.find_opt x = Some gx
		      	   so mgx = replace v u gx
			   so mem x gx

       lemma merge_arrow_right :
       	     forall v u g a. sym g -> irrefl g -> mem u (domain g) -> mem v (domain g) ->
	     	      	     v <> u -> not mem_dic v g u ->
			     (mem_dic u (merge v u g) a <-> mem_dic u g a \/ mem_dic v g a)
	     by (mem_dic u (merge v u g) a -> exists mga. (merge v u g).find_opt a = Some mga
	     	so not a = u /\ not a = v
		so exists ga. g.find_opt a = Some ga
		so mga = replace v u ga) /\
		(mem_dic u g a -> mem_dic u (merge v u g) a
		by not a = u /\ not a = v
		so exists mga. (merge v u g).find_opt a = Some mga
		so exists ga. g.find_opt a = Some ga
		so mga = replace v u ga) /\
		(mem_dic v g a -> mem_dic u (merge v u g) a
		by not a = u /\ not a = v
		so exists mga. (merge v u g).find_opt a = Some mga
		so exists ga. g.find_opt a = Some ga
		so mga = replace v u ga)


       lemma merge_arrow_left :
       	     forall v u g a. sym g -> irrefl g -> mem u (domain g) -> mem v (domain g) ->
	     	      	     v <> u -> not mem_dic v g u ->
			     (mem_dic a (merge v u g) u <-> mem_dic a g u \/ mem_dic a g v)
             by	exists gu gv. g.find_opt u = Some gu /\ g.find_opt v = Some gv
	     so (mem_dic a (merge v u g) u -> exists mgu. (merge v u g).find_opt u = Some mgu
		so mem a (replace v u gu) \/ mem a (replace v u gv)) /\
		(mem_dic a g v -> not a = v
		so mem a gv
		so mem a (replace v u gv))

       lemma sym_merge :
       	     forall v u g. sym g -> irrefl g -> mem u (domain g) -> mem v (domain g) ->
	     	      	   v <> u -> not mem_dic v g u ->
			   sym (merge v u g)
	     by (mem_dic u g v -> false by mem_dic v g u)
		so (forall a b. mem_dic a (merge v u g) b -> mem_dic b (merge v u g) a
		   by exists mgb. (merge v u g).find_opt b = Some mgb
		      so not b = v
		      so if b = u
		      	 then mem_dic a g u \/ mem_dic a g v
		      	   so mem_dic u g a \/ mem_dic v g a
			 else exists gb. g.find_opt b = Some gb
			   so mgb = replace v u gb
			   so not a = v
			   so if a = u
			      then mem_dic u g b \/ mem_dic v g b
			     	so mem_dic b g u \/ mem_dic b g v
			      else mem_dic b g a
			      	so exists ga. g.find_opt a = Some ga
			     	so mem b ga
			     	so mem b (replace v u ga)
			     	so mem_dic b (merge v u g) a)



       lemma no_collision_merge :
       	     forall v u g a au.
	     sym g -> irrefl g -> mem u (domain g) -> mem v (domain g) ->
	     v <> u -> not mem_dic v g u -> a.find_opt u = Some au ->
	     no_collision (merge v u g) a ->
	     let na = add_binding v au a in
	     no_collision g na
	     by forall x y nax nay. mem_dic y g x ->
		 	       	     na.find_opt x = Some nax ->
	   	 	       	     na.find_opt y = Some nay -> nax = nay -> false
		by if x = v
		   then mem_dic y (merge v u g) u
		   else if y = v
		   	then mem_dic x (merge v u g) u
			else mem_dic y (merge v u g) x



       let rec irc (g : Dic.t (set var)) (r : set var) : Dic.t var
       	   requires { forall v. mem v r -> mem v (domain g) }
      	   requires { irrefl g }
      	   requires { sym g }
      	   raises { Dic.Not_found -> false }
      	   variant { cardinal (domain g) }
      	   ensures { forall v u. result.find_opt v = Some u -> mem u (domain g) }
      	   ensures { domain result = domain g }
      	   ensures { forall k. mem k r -> result.find_opt k = Some k }
      	   ensures { no_collision g result }
       =
	   match pick g r with
      	   | None -> Dic.init r
      	   | Some v -> match coalesce g v with
      	       	       | None ->
  		       	 let s = Dic.find v g in
		    	 let a = irc (remove v g) r in
			 match available s a r with
			 | Some av -> Dic.add_binding v av a
			 | None -> Dic.add_binding v v a end
		       | Some u ->
  		       	 let a = irc (merge v u g) r in
  		    	 Dic.add_binding v (Dic.find u a) a
		       end
           end


end
