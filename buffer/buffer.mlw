use int.Int
use list.ListRich


type buf 'a = { h : int; xs : list 'a }

let rec function take n xs
    variant { xs }
    = match n, xs with
    | _, Nil -> Nil
    | n, Cons x rs ->
      	 if n = 0 then Nil
	 else Cons x (take (n-1) rs)
    end

let rec function split n xs
    returns { a, b -> a = take n xs /\ xs = a ++ b /\ (length a = n \/ b = Nil) }
    variant { xs }
    = match n, xs with
    | _, Nil -> Nil, Nil
    | n, Cons x rs ->
      	 if n = 0 then (Nil, xs)
	 else let s1, s2 = split (n-1) rs in
	      (Cons x s1, s2)
    end

let rec lemma take_append (l1 l2 : list 'a) (n : int)
    requires { 0 <= n <= length l1 }
    ensures { take n (l1 ++ l2) = take n l1 }
  =
    match l1 with
    | Nil -> ()
    | Cons _ t1 -> if n <> 0 then take_append t1 l2 (n-1)
    end

let rec lemma take_less (n : int) (l : list 'a)
    requires { n >= 0 }
    ensures { length (take n l) <= n }
    = match l with
      | Nil -> ()
      | Cons _ l -> if n <> 0 then take_less (n-1) l
      end

let lemma take_weakening (n : int) (l1 l2 : list 'a)
    	requires { 0 <= n }
	requires { take (n+1) l1 = take (n+1) l2 }
	ensures { take n l1 = take n l2 }
  = let _ = split (n+1) l1 in
    split (n+1) l2

lemma take_cons :
      forall n, a : 'a, l1 l2. 0 <= n -> take n l1 = take n l2 ->
      take n (Cons a l1) = take n (Cons a l2)



(* Naive implementation *)
let function empty h = { h = h; xs = Nil }

let function get buf =
    let { h = h; xs = xs } = buf in
    take h xs

let function add x buf =
    ensures { result.h = buf.h }
    ensures { get result = take buf.h (Cons x buf.xs) }
    let { h = h; xs = xs } = buf in
    { h = h; xs = Cons x xs }



(* Caterpillar implementation *)
type buf_caterpillar 'a = { h_c : int; xs_c : list 'a; xs_len : int; ys : list 'a }

predicate well_formed_c (buf : buf_caterpillar 'a) =
	  let { h_c = h; xs_c = xs; xs_len = len; ys = ys } = buf in
	  h >= 0 /\ length xs = len /\ (length ys = 0 \/ length ys = h)


let function empty_c h = { h_c = h; xs_c = Nil; xs_len = 0; ys = Nil }

let function get_c buf =
    let { h_c = h; xs_c = xs; xs_len = _; ys = ys } = buf in
    take h (xs ++ ys)

predicate equal_nc (b : buf 'a) (bc : buf_caterpillar 'a) =
	  b.h = bc.h_c /\ get b = get_c bc

let function add_c x buf
    requires { well_formed_c buf }
    ensures { result.h_c = buf.h_c }
    ensures { well_formed_c result }
    ensures { forall b. equal_nc b buf -> equal_nc (add x b) result }
=
    let { h_c = h; xs_c = xs; xs_len = xs_len ; ys = ys } = buf in
    if xs_len = h - 1
    then { h_c = h; xs_c = Nil; xs_len = 0; ys = Cons x xs }
    else { h_c = h; xs_c = Cons x xs; xs_len = xs_len + 1; ys = ys }



(* Correction *)
lemma get_empty :
      forall h. h >= 0 ->
      let eh : buf 'a = empty h in
      let eh_c : buf_caterpillar 'a = empty_c h in
      equal_nc eh eh_c /\ well_formed_c eh_c

lemma get_add :
      forall x : 'a, b bc. equal_nc b bc /\ well_formed_c bc ->
      let axb = add x b in
      let axbc = add_c x bc in
      well_formed_c axbc /\ equal_nc axb axbc
